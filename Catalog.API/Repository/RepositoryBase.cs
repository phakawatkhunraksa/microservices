﻿using Catalog.API.Code;
using Catalog.API.Models;
using Dapper;
using Microsoft.Extensions.Options;
using MySql.Data.MySqlClient;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Text;

namespace Catalog.API.Repository
{
    public class RepositoryBase : IRepositoryBase
    {
        private readonly IDbConnection _db;

        public RepositoryBase(IOptions<ConnectionStringList> connectionStrings)
        {
            _db = new MySqlConnection(connectionStrings.Value.ConnectionString1);
        }

        public void Dispose()
        {
            _db.Close();
        }

        public async Task<List<Cars>> SearchCars(int pageSize, int pageIndex, FilterCar filter)
        {
            // SQL
            var sql = new StringBuilder();
            // select
            sql.Append(@"SELECT car_key, car_name, car_price_sell, car_cover ");
            // from
            sql.Append(@"FROM cars ");
            // where
            sql.Append(@"WHERE True ");
            sql.Append($"{(string.IsNullOrEmpty(filter.brand) ? filter.brand : "AND car_brand='" + filter.brand + "' ")}");
            sql.Append($"{(string.IsNullOrEmpty(filter.gen) ? filter.gen : "AND car_gen='" + filter.gen + "' ")}");
            sql.Append($"{(string.IsNullOrEmpty(filter.color) ? filter.color : "AND car_color='" + filter.color + "' ")}");
            // order
            sql.Append($"ORDER BY car_name ASC ");
            // paging
            sql.Append($"LIMIT 1,{pageSize * (pageIndex + 1)}");
            // .end SQL

            var result = await _db.QueryAsync<Cars>(sql.ToString());
            return result.ToList();
        }
        public async Task<int> CountSearchCars(FilterCar filter)
        {
            // SQL
            var sql = new StringBuilder();
            // select
            sql.Append(@"SELECT count(0) ");
            // from
            sql.Append(@"FROM cars ");
            // where
            sql.Append(@"WHERE True ");
            sql.Append($"{(string.IsNullOrEmpty(filter.brand) ? filter.brand : "AND car_brand='" + filter.brand + "' ")}");
            sql.Append($"{(string.IsNullOrEmpty(filter.gen) ? filter.gen : "AND car_gen='" + filter.gen + "' ")}");
            sql.Append($"{(string.IsNullOrEmpty(filter.color) ? filter.color : "AND car_color='" + filter.color + "' ")}");
            // .end SQL

            var result = await _db.QuerySingleAsync<int>(sql.ToString());
            return result;
        }

        public async Task<List<Cars>> GetAllCars(int pageSize, int pageIndex)
        {
            var sql = new StringBuilder();
            sql.Append(@"SELECT car_key, car_name, car_price_sell, car_cover FROM cars ORDER BY car_name ASC ");
            sql.AppendFormat(@"LIMIT 1,{0}", pageSize*(pageIndex+1));
            var result = await _db.QueryAsync<Cars>(sql.ToString());
            return result.ToList();
        }
        public async Task<int> CountAllCars()
        {
            var sql = new StringBuilder();
            sql.Append(@"SELECT count(0) FROM cars");
            var result = await _db.QuerySingleAsync<int>(sql.ToString());
            return result;
        }
        // Car Brands List for Dropdown-List
        public async Task<List<CarBrands>> CarBrandsList()
        {
            var sql = new StringBuilder();
            sql.Append(@"SELECT brand_key, brand_title ");
            sql.Append(@"FROM car_brands ");
            sql.Append(@"WHERE brand_status");
            var result = await _db.QueryAsync<CarBrands>(sql.ToString());
            return result.ToList();
        }

        public async Task<List<CarGen>> CarGenByBrand(string brand)
        {
            var sql = new StringBuilder();
            sql.Append("SELECT gen_key, gen_title ");
            sql.Append("FROM car_gen ");
            sql.Append($"WHERE gen_status AND {(string.IsNullOrEmpty(brand) ? "False" : "brand_key='" + brand+"'")}");
            
            var result = await _db.QueryAsync<CarGen>(sql.ToString());
            return result.ToList();
        }

        // Car Color List for Dropdown-List
        public async Task<List<CarColor>> CarColorList()
        {
            var sql = new StringBuilder();
            sql.Append(@"SELECT color_key, color_title ");
            sql.Append(@"FROM car_color ");
            sql.Append(@"WHERE color_status");
            var result = await _db.QueryAsync<CarColor>(sql.ToString());
            return result.ToList();
        }
    }
}
