﻿using Catalog.API.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Catalog.API.Repository
{
    public interface IRepositoryBase
    {
        
        Task<List<Cars>> GetAllCars(int pageSize, int pageIndex);
        Task<int> CountAllCars();

        Task<List<Cars>> SearchCars(int pageSize, int pageIndex, FilterCar filter);
        Task<int> CountSearchCars(FilterCar filter);

        Task<List<CarBrands>> CarBrandsList();
        Task<List<CarGen>> CarGenByBrand(string brand);
        
        Task<List<CarColor>> CarColorList();
    }
}
