﻿using Dapper.Contrib.Extensions;

namespace Catalog.API.Models
{
    [Table("cars")]
    public class Cars
    {
        [Key]
        public string car_key { get; set; }
        public string car_name { get; set; }
        public string car_cover { get; set; }
        public double car_price_sell { get; set; }
    }
}
