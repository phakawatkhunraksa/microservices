﻿using Dapper.Contrib.Extensions;

namespace Catalog.API.Models
{
    [Table("car_color")]
    public class CarColor
    {
        [Key]
        public string color_key { get; set; }
        public string color_title { get; set; }
    }
}
