﻿

using Dapper.Contrib.Extensions;

namespace Catalog.API.Models
{
    [Table("car_gen")]
    public class CarGen
    {
        [Key]
        public string gen_key { get; set; }
        public string gen_title { get; set; }
    }
}
