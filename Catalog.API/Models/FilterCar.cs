﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Catalog.API.Models
{
    public class FilterCar
    {
        public string brand { get; set; }
        public string gen { get; set; }
        public string color { get; set; }
    }
}
