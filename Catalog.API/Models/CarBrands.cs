﻿using Dapper.Contrib.Extensions;

namespace Catalog.API.Models
{
    [Table("car_brands")]
    public class CarBrands
    {
        [Key]
        public string brand_key { get; set; }
        public string brand_title { get; set; }
    }
}
