﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Catalog.API.ViewModel;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Catalog.API.Code;
using Catalog.API.Models;
using Catalog.API.Repository;
using Newtonsoft.Json;

namespace Catalog.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CatalogController : ControllerBase
    {
        public IRepositoryBase _repository;

        public CatalogController(IOptions<ConnectionStringList> connectionStrings)
        {
            this._repository = new RepositoryBase(connectionStrings);
        }

        // GET api/[controller]/items[?pageSize=3&pageIndex=10&filter={}]
        [HttpGet]
        [Route("items")]
        [ProducesResponseType(typeof(PaginatedItemsViewModel<Cars>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(IEnumerable<Cars>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> ItemsAsync([FromQuery]int pageSize = 10, [FromQuery]int pageIndex = 0, [FromQuery]string filter = null)
        {
            if (filter != null)
            {
                var _filter = JsonConvert.DeserializeObject<FilterCar>(filter);

                var totalItems = await _repository.CountSearchCars(_filter);
                var itemsOnPage = await _repository.SearchCars(pageSize, pageIndex, _filter);

                var model = new PaginatedItemsViewModel<Cars>(pageIndex, pageSize, totalItems, itemsOnPage);

                return Ok(model);
            }
            else
            {
                var totalItems = await _repository.CountAllCars();
                var itemsOnPage = await _repository.GetAllCars(pageSize, pageIndex);

                var model = new PaginatedItemsViewModel<Cars>(pageIndex, pageSize, totalItems, itemsOnPage);

                return Ok(model);
            }
        }

        // GET api/[controller]/CarBrands
        [HttpGet]
        [Route("carbrands")]
        [ProducesResponseType(typeof(List<CarBrands>), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<List<CarBrands>>> CarBrandsAsync()
        {
            return await _repository.CarBrandsList();
        }

        // GET api/[controller]/CarGens
        [HttpGet]
        [Route("cargens")]
        [ProducesResponseType(typeof(List<CarBrands>), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<List<CarGen>>> CarGensAsync(string brand)
        {
            return await _repository.CarGenByBrand(brand);
        }

        // GET api/[controller]/CarColors
        [HttpGet]
        [Route("carcolors")]
        [ProducesResponseType(typeof(List<CarColor>), (int)HttpStatusCode.OK)]
        public async Task<ActionResult<List<CarColor>>> CarColorsAsync()
        {
            return await _repository.CarColorList();
        }

        
    }
    
}