﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using WebMVC.Infrastructure;
using WebMVC.ViewModels;
using WebMVC.ViewModels.CatalogViewModels;

namespace WebMVC.Services
{
    public class CatalogService : ICatalogService
    {
        private readonly IOptions<AppSettings> _settings;
        private readonly HttpClient _httpClient;
        private readonly ILogger<CatalogService> _logger;

        private readonly string _remoteServiceBaseUrl;

        public CatalogService(HttpClient httpClient, ILogger<CatalogService> logger, IOptions<AppSettings> settings)
        {
            _httpClient = httpClient;
            _settings = settings;
            _logger = logger;

            _remoteServiceBaseUrl = $"https://localhost:44348/api/catalog/";//$"{_settings.Value.PurchaseUrl}/api/v1/c/catalog/";
        }

        public async Task<Catalog> GetCatalogItems(int page, int take, IndexViewModel model)
        {
            var uri = API.Catalog.GetAllCatalogItems(_remoteServiceBaseUrl, page, take, model);

            var responseString = await _httpClient.GetStringAsync(uri);

            var catalog = JsonConvert.DeserializeObject<Catalog>(responseString);

            return catalog;
        }
        
        public async Task<IEnumerable<SelectListItem>> GetBrands()
        {
            var uri = API.Catalog.GetAllBrands(_remoteServiceBaseUrl);

            var responseString = await _httpClient.GetStringAsync(uri);

            var items = new List<SelectListItem>();

            items.Add(new SelectListItem() { Value = "", Text = "ทุกยี่ห้อ", Selected = true });

            var brands = JArray.Parse(responseString);

            foreach (var brand in brands.Children<JObject>())
            {
                items.Add(new SelectListItem()
                {
                    Value = brand.Value<string>("brand_key"),
                    Text = brand.Value<string>("brand_title")
                });
            }

            return items;
        }
        public async Task<IEnumerable<SelectListItem>> GetGens(string brand_key)
        {
            var uri = API.Catalog.GetGensByBrand(_remoteServiceBaseUrl, brand_key);

            var responseString = await _httpClient.GetStringAsync(uri);

            var items = new List<SelectListItem>();
            items.Add(new SelectListItem() { Value = "", Text = "ทุกรุ่น", Selected = true });

            var gens = JArray.Parse(responseString);
            foreach (var gen in gens.Children<JObject>())
            {
                items.Add(new SelectListItem()
                {
                    Value = gen.Value<string>("gen_key"),
                    Text = gen.Value<string>("gen_title")
                });
            }

            return items;
        }
        public async Task<IEnumerable<SelectListItem>> GetColors()
        {
            var uri = API.Catalog.GetAllColors(_remoteServiceBaseUrl);

            var responseString = await _httpClient.GetStringAsync(uri);

            var items = new List<SelectListItem>();
            items.Add(new SelectListItem() { Value = "", Text = "ทุกสี", Selected = true });

            var colors = JArray.Parse(responseString);
            foreach (var color in colors.Children<JObject>())
            {
                items.Add(new SelectListItem()
                {
                    Value = color.Value<string>("color_key"),
                    Text = color.Value<string>("color_title")
                });
            }

            return items;
        }
    }
}
