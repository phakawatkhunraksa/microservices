﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebMVC.ViewModels.Pagination;

namespace WebMVC.ViewModels.CatalogViewModels
{
    public class IndexViewModel
    {
        public IEnumerable<CatalogItem> CatalogItems { get; set; }
        public IEnumerable<SelectListItem> Brands { get; set; }
        public IEnumerable<SelectListItem> Gens { get; set; }
        public IEnumerable<SelectListItem> Colors { get; set; }
        public string BrandFilterApplied { get; set; }
        public string GensFilterApplied { get; set; }
        public string ColorsFilterApplied { get; set; }
        public PaginationInfo PaginationInfo { get; set; }
    }
}
