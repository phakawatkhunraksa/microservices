﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebMVC.ViewModels
{
    public class CatalogItem
    {
        public string car_key { get; set; }
        public string car_name { get; set; }
        public string car_cover { get; set; }
        public double car_price_sell { get; set; }
    }
}
