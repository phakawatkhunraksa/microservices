﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebMVC.Services;
using WebMVC.ViewModels.CatalogViewModels;
using WebMVC.ViewModels.Pagination;

namespace WebMVC.Controllers
{
    public class CatalogController : Controller
    {
        private ICatalogService _catalogService;

        public CatalogController(ICatalogService catalogService)=>
            _catalogService = catalogService;

        public async Task<IActionResult> Index(IndexViewModel model, int? page, [FromQuery]string errorMsg)
        {
            var itemsPage = 10;
            var catalog = await _catalogService.GetCatalogItems(page ?? 0, itemsPage, model);
            var vm = new IndexViewModel()
            {
                CatalogItems = catalog.Data,
                Brands = await _catalogService.GetBrands(),
                Gens = await _catalogService.GetGens(model.BrandFilterApplied),
                Colors = await _catalogService.GetColors(),
                BrandFilterApplied = model.BrandFilterApplied ?? string.Empty,
                GensFilterApplied = model.GensFilterApplied ?? string.Empty,
                ColorsFilterApplied = model.ColorsFilterApplied ?? string.Empty,
                PaginationInfo = new PaginationInfo()
                {
                    ActualPage = page ?? 0,
                    ItemsPerPage = catalog.Data.Count,
                    TotalItems = catalog.Count,
                    TotalPages = (int)Math.Ceiling(((decimal)catalog.Count / itemsPage))
                }
            };

            vm.PaginationInfo.Next = (vm.PaginationInfo.ActualPage == vm.PaginationInfo.TotalPages - 1) ? "is-disabled" : "";
            vm.PaginationInfo.Previous = (vm.PaginationInfo.ActualPage == 0) ? "is-disabled" : "";

            ViewBag.BasketInoperativeMsg = errorMsg;

            return View(vm);
        }

        public async Task<IActionResult> getCarGens(string brand_key)
        {
            var gens = await _catalogService.GetGens(brand_key);
            return Json(gens);
        }
    }
}